using System;

namespace CoolParking.BL.Common.Helpers
{
    public static class RandomNumberStringHelper
    {
        public static string GenerateRandomNumberString(int length)
        {
            var result = "";
            var random = new Random();

            for (var i = 0; i < length; i++)
            {
                result += random.Next(0, 9).ToString();
            }
            
            return result;
        }
    }
}