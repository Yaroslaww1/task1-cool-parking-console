using System;

namespace CoolParking.BL.Common.Helpers
{
    public static class RandomUppercaseCharHelper
    {
        public static char GenerateRandomUppercaseChar()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var random = new Random();

            return chars[random.Next(chars.Length)];
        }
    }
}