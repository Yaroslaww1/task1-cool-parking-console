﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using System.Timers;
using CoolParking.BL.Interfaces;

public class TimerService : ITimerService
{
    public event ElapsedEventHandler Elapsed;
    public double Interval { get; set; }
    private readonly Timer _timer;

    public TimerService(ElapsedEventHandler eventHandler, double interval)
    {
        Elapsed = eventHandler;
        Interval = interval;

        _timer = new Timer(interval) {AutoReset = true};
        _timer.Elapsed += Elapsed;
    }
    
    public void Start()
    {
        _timer.Start();
    }

    public void Stop()
    {
        _timer.Stop();
    }

    public void Dispose()
    {
        _timer.Dispose();
    }
}