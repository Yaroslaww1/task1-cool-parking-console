﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public sealed class ParkingService : IParkingService
    {
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;

        private readonly Parking _parking;
        private readonly List<TransactionInfo> _transactions;
        
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Elapsed += _withdraw;
            _logTimer = logTimer;
            _logTimer.Elapsed += _saveTransactions;
            _logService = logService;

            _parking = new Parking();
            _transactions = new List<TransactionInfo>();
            
            _withdrawTimer.Start();
            _logTimer.Start();
        }
        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
        }

        private void _withdraw(object o, ElapsedEventArgs e)
        {
            foreach (var vehicle in _parking.Vehicles.Values)
            {
                var withdrawRateFromBalance = vehicle.WithdrawRateFromBalance();
                _parking.Balance += withdrawRateFromBalance;
                _transactions.Add(new TransactionInfo(vehicle.Id, withdrawRateFromBalance));
            }
        }

        private void _saveTransactions(object o, ElapsedEventArgs e)
        {
            _logService.Write(string.Join(",", _transactions.ToArray().Select(t => t.ToString()) ));
            _transactions.Clear();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.GetFreePlaces();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.GetVehicles();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            _parking.AddVehicle(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            _parking.RemoveVehicle(vehicleId);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            _parking.TopUpVehicle(vehicleId, sum);
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }
    }
}