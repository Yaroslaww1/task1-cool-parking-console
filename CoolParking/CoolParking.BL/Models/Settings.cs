﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal StartingParkingBalance { get; } = 0;
        public static int ParkingCapacity { get; } = 10;
        public static int WithdrawPeriodInSeconds { get; } = 5;
        public static int TransactionLogWritePeriodInSeconds { get; } = 60;
        public static decimal PassengerCarRate { get; } = 2;
        public static decimal TruckRate { get; } = 5;
        public static decimal BusRate { get; } = 3.5m;
        public static decimal MotorcycleRate { get; } = 1;
        public static decimal PenaltyRate { get; } = 2.5m;
        public static string TransactionLogPath { get; } = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
    }
}