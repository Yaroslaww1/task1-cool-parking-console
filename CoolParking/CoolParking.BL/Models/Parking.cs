﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public decimal Balance;
        public readonly int Capacity;
        
        public readonly Dictionary<string, Vehicle> Vehicles;

        public Parking()
        {
            Balance = Settings.StartingParkingBalance;
            Capacity = Settings.ParkingCapacity;
            
            Vehicles = new Dictionary<string, Vehicle>();
        }
        
        public int GetFreePlaces()
        {
            return Capacity - Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new(Vehicles.Values.ToList());
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (Vehicles.Count == Capacity)
            {
                throw new InvalidOperationException("Cannot add a vehicle: parking is already full");
            }
            
            Vehicles.Add(vehicle.Id, vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var isVehicleExists = Vehicles.ContainsKey(vehicleId);
            if (!isVehicleExists)
            {
                throw new ArgumentException($"Vehicle with id={vehicleId} was not found");
            }

            var vehicle = Vehicles[vehicleId];

            if (vehicle.Balance < 0)
            {
                throw new InvalidOperationException("Cannot remove a vehicle with a negative balance");
            }
            
            Vehicles.Remove(vehicleId);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var isVehicleExists = Vehicles.ContainsKey(vehicleId);
            if (!isVehicleExists)
            {
                throw new ArgumentException($"Vehicle with id={vehicleId} was not found");
            }

            var vehicle = Vehicles[vehicleId];

            vehicle.TopUpBalance(sum);
        }
    }
}