﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;
using CoolParking.BL.Common.Helpers;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public readonly string Id;
        public readonly VehicleType VehicleType;
        public decimal Balance;

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            _validateId(id);
            _validateBalance(balance);
            
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        private static void _validateId(string id)
        {
            var regex = new Regex("^.*-[0-9]+-.*$");
            // Only Ids like AA-0001-AA
            var isValid = regex.IsMatch(id) && id.Length == 10;

            if (!isValid)
            {
                throw new ArgumentException("Vehicle Id is not valid");
            }
        }

        private static void _validateBalance(decimal balance)
        {
            var isValid = balance >= 0;

            if (!isValid)
            {
                throw new ArgumentException("Vehicle Balance is not valid");
            }
        }

        public void TopUpBalance(decimal amount)
        {
            if (amount < 0)
            {
                throw new ArgumentException("TopUp balance cannot be negative");
            }

            Balance += amount;
        }

        public decimal WithdrawFromBalance(decimal amount)
        {
            switch (Balance)
            {
                case > 0 when Balance > amount:
                    Balance -= amount;
                    return amount;
                case > 0 when Balance < amount:
                    amount -= Balance;
                    Balance = -1 * (amount * Settings.PenaltyRate);
                    return amount * Settings.PenaltyRate;
                case < 0:
                    Balance = -1 * (amount * Settings.PenaltyRate);
                    return amount * Settings.PenaltyRate;
            }

            return 0;
        }
        
        public decimal WithdrawRateFromBalance()
        {
            switch (VehicleType)
            {
                case VehicleType.Bus:
                    return WithdrawFromBalance(Settings.BusRate);
                case VehicleType.Motorcycle:
                    return WithdrawFromBalance(Settings.MotorcycleRate);
                case VehicleType.PassengerCar:
                    return WithdrawFromBalance(Settings.PassengerCarRate);
                case VehicleType.Truck:
                    return WithdrawFromBalance(Settings.TruckRate);
            }

            return 0;
        }
        
        static string GenerateRandomRegistrationPlateNumber()
        {
            return RandomUppercaseStringHelper.GenerateRandomUppercaseString(2) +
                "-" +
                RandomNumberStringHelper.GenerateRandomNumberString(4) +
                "-" +
                RandomUppercaseStringHelper.GenerateRandomUppercaseString(2);
        }
    }
}